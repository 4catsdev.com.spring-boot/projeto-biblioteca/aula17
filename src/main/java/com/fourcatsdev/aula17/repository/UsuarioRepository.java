package com.fourcatsdev.aula17.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula17.modelo.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	Usuario findByLogin(String login);
}
